# README #

How to run Android app.

### React Native (Android) starter guide ###
1. Connect Android phone to PC with USB.
2. Run in cmd: adb reverse tcp:3000 tcp:3000 (Run this commad from everywhere c:\ directory etc.)
3. Run in cmd: npx json-server --watch data/data.json --port 3000 (Run this comand from Project directory)
4. Run in cmd: npx react-native start (Run this comand from Project directory)
5. Run in cmd: npx react-native run-android (Run this comand from Project directory)

### App screenshots ###
HomeScreen
![screen_1](./screenshots/screen_1.png)

DetailScreen
![screen_2](./screenshots/screen_2.png)