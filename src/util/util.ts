

export const truncateText = (text: string) => {
  return text.length > 16 ? `${text.substring(0, 16)}...` : text;
};
