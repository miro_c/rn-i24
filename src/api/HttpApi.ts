import {BASE_URL, REQUEST_PARAM_CAROUSELS} from './httpConstants';

export const HttpApi = {

  /**  GET /movies **/
  getMoviesAsync: async (): Promise<Response> => {
    const response = await fetch(BASE_URL + REQUEST_PARAM_CAROUSELS);
    return response;
  }
  
};
