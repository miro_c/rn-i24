
export interface Carousel {
  title: string;
  items: Item[];
}

export interface Item {
  id: number;
  title: string;
  year: string;
  duration: string;
  completedDate: string;
  genres: string[];
  director: string;
  actors: string;
  plot: string;
  posterUrl: string;
}