import { createStore } from 'redux';
import { rootReducer } from './state';

const getStore = () => {
  return createStore(rootReducer, {});
};

export default getStore;