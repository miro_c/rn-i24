import {Dispatch} from 'react';
import {ActionEnum, StateEnum} from './reducer';
import {Carousel} from '../model/Carousel';

export const dispatchLoadDataSuccess = async (
  dispatch: Dispatch<any>,
  data: Carousel[],
) => {
  dispatch({
    type: ActionEnum.LOAD_MOVIES,
    payload: {
      state: StateEnum.SUCCESS,
      data: data,
    },
  });
};

export const dispatchLoadDataError = async (dispatch: Dispatch<any>) => {
  dispatch({
    type: ActionEnum.LOAD_MOVIES,
    payload: {
      state: StateEnum.ERROR,
      data: [],
    },
  });
};

export const dispatchLoadDataLoading = async (dispatch: Dispatch<any>) => {
  dispatch({
    type: ActionEnum.LOAD_MOVIES,
    payload: {
      state: StateEnum.LOADING,
      data: [],
    },
  });
};
