import { combineReducers } from 'redux';
import { MovieReducer } from "./reducer";

export const rootReducer = combineReducers({
  movieState: MovieReducer
});

export type State = ReturnType<typeof rootReducer>;