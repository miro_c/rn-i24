import {Carousel} from '../model/Carousel';

export enum StateEnum {
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export enum ActionEnum {
  LOAD_MOVIES = 'LOAD_MOVIES',
  DETAIL_MOVIE = 'DETAIL_MOVIE',
}

export interface ReducerAction {
  type: ActionEnum;
  payload: MovieState;
}

export interface MovieState {
  state: StateEnum;
  data: Carousel[];
}

const defaultAppState: MovieState = {
  state: StateEnum.LOADING,
  data: [],
} as MovieState;

export const MovieReducer = (
  state: MovieState = defaultAppState,
  action: ReducerAction,
): MovieState | null => {
  switch (action.type) {
    case ActionEnum.LOAD_MOVIES:
      return {
        state: action.payload?.state,
        data: action.payload?.data,
      };

    case ActionEnum.DETAIL_MOVIE:
      return {
        state: action.payload?.state,
        data: action.payload?.data,
        // TODO
      };

    default:
      return state;
  }
};
