import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  StyleSheet,
  Dimensions,
  ScaledSize,
} from 'react-native';
import {Item} from '../model/carousel';

const DetailScreen = ({route}: {route: any}) => {

  /** Cast Item from route param **/
  const movie = route.params.movieDetail as Item;

  const [dimensions, setDimensions] = useState<ScaledSize>(
    Dimensions.get('window'),
  );

  const [scaleFactorY, setScaleFactorY] = useState<number>(1.0);

  /** Find screen dimension for ScrollView height **/
  useEffect(() => {
    const onDimensionsChange = ({window}: {window: ScaledSize}) => {
      setDimensions(window);
    };

    const susbcription = Dimensions.addEventListener(
      'change',
      onDimensionsChange,
    );

    if (dimensions.width > dimensions.height) {
      setScaleFactorY(
        (dimensions.width / dimensions.height) * dimensions.scale,
      );
    } else {
      setScaleFactorY(1.0);
    }

    return () => susbcription.remove();
  });

  return (
    <View style={styles.content}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          height: dimensions.height * scaleFactorY,
          width: dimensions.width,
        }}
        showsHorizontalScrollIndicator={true}>
        <ImageBackground style={styles.thumb} source={{uri: movie.posterUrl}}>
          <Text style={styles.title}>{movie.title}</Text>
        </ImageBackground>
        <View style={styles.plot}>
          <Text style={styles.subtitle}>{movie.plot}</Text>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#002024',
  },
  thumb: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  plot: {
    padding: 16,
  },
  title: {
    fontSize: 44,
    fontWeight: '900',
    color: '#ffffff',
    padding: 20,
    backgroundColor: '#002024ba',
  },
  subtitle: {
    fontSize: 16,
    color: '#ffffff',
  },
});

export default DetailScreen;
