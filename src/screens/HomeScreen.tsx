import React from 'react';
import {View} from 'react-native';
import Header from '../../src/components/Header';
import MovieList from '../../src/components/MovieList';
import {StyleSheet} from 'react-native';

const HomeScreen: React.FC = () => {
  return (
    <View style={styles.content}>
      <Header title="i24 Movie" subTitle="Popular movies" />
      <MovieList />
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    backgroundColor: '#003941',
  },
});

export default HomeScreen;
