import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Logo from '../res/img/ic_logo.svg';

interface Props {
  title: string;
  subTitle: string;
}

const Header: React.FC<Props> = ({title, subTitle}) => {
  return (
    <View style={styles.header}>
      <Logo style={styles.logo} width={70} height={70} />
      <View>
        <Text style={styles.headerText}>{title}</Text>
        <Text style={styles.headerSubText}>{subTitle}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    backgroundColor: '#003941',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
  },
  logo: {
    marginRight: 10,
  },
  headerText: {
    fontSize: 30,
    fontWeight: '800',
    color: 'rgb(255, 255, 255)',
  },
  headerSubText: {
    fontSize: 15,
    fontWeight: '800',
    color: '#3DDC84',
    marginTop: -3,
  },
});

export default Header;
