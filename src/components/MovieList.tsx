import React, {useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {State} from '../redux/state';
import {MovieState, StateEnum} from '../redux/reducer';
import {
  dispatchLoadDataSuccess,
  dispatchLoadDataError,
  dispatchLoadDataLoading,
} from '../redux/dispatcher';
import {useSelector, useDispatch} from 'react-redux';
import {Item} from '../model/carousel';
import {HttpApi} from '../api/httpApi';
import MovieItem from '../../src/components/MovieItem';
import ErrorImg from '../res/img/ic_logo_error.svg';

const MovieList = () => {
  const dispatch = useDispatch();

  /** Get current state from redux **/
  const movieState = useSelector(
    (state: State) => state.movieState,
  ) as MovieState;

  /** Http Get movies initial request  **/
  useEffect(() => {
    dispatchLoadDataLoading(dispatch);

    HttpApi.getMoviesAsync()
      .then(async response => {
        const carousels = await response.json();
        dispatchLoadDataSuccess(dispatch, carousels);
      })
      .catch(() => {
        dispatchLoadDataError(dispatch);
      });
  }, []);

  /** Render UI State **/
  switch (movieState.state) {
    case StateEnum.LOADING:
      return (
        <View style={styles.loading}>
          <ActivityIndicator
            size="large"
            style={styles.progress}
            color="#3DDC84"
          />
        </View>
      );

    case StateEnum.SUCCESS:
      return (
        <ScrollView
          style={styles.content}
          contentInsetAdjustmentBehavior="automatic">
          {movieState.data?.map(movie => (
            <View key={movie.title}>
              <Text style={styles.title}>{movie.title}</Text>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {movie?.items.map((item: Item) => {
                  return <MovieItem key={item.id} movieItem={item} />;
                })}
              </ScrollView>
            </View>
          ))}
        </ScrollView>
      );

    case StateEnum.ERROR:
      return (
        <View style={styles.loading}>
          <View style={styles.errorContainer}>
            <ErrorImg style={styles.errorImg} width={120} height={120} />
            <Text style={styles.error}>Loading data error :(</Text>
          </View>
        </View>
      );
  }
};

const styles = StyleSheet.create({
  content: {
    marginBottom: 100,
  },
  title: {
    fontSize: 18,
    color: '#fff',
  },
  loading: {
    width: '100%',
    height: '100%',
    backgroundColor: '#003941',
  },
  progress: {
    width: '100%',
    height: '70%',
  },
  errorContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '70%',
  },
  errorImg: {
    justifyContent: 'center',
  },
  error: {
    fontSize: 14,
    color: '#ec3b3b',
    textAlign: 'center',
    fontWeight: '400',
  },
});

export default MovieList;
