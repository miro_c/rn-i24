import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import { SCREEN_DETAIL }  from '../screens/routeConstant';
import {Item} from '../model/carousel';
import {truncateText} from '../util/util';

interface Props {
  movieItem: Item;
}

type NavigationParamList = {
  Detail: {movieDetail: Item};
};

const MovieItem: React.FC<Props> = ({movieItem}: Props) => {
  const navigation = useNavigation<StackNavigationProp<NavigationParamList>>();

  /** Navigate to Detail Screen **/
  const showDetailMovie = async (itemMovie: Item) => {
    navigation.navigate(SCREEN_DETAIL, {movieDetail: itemMovie});
  };

  return (
    <TouchableOpacity onPress={() => showDetailMovie(movieItem)}>
      <View style={styles.item}>
        <Image style={styles.thumb} source={{uri: movieItem.posterUrl}} />
        <Text ellipsizeMode='tail' numberOfLines={2}  style={styles.title}>{truncateText(movieItem.title)}</Text>
        <Text style={styles.subTitle}>({movieItem.year})</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  item: {
    alignItems: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap',
    padding: 20,
    marginVertical: 8,
    backgroundColor: '#002c31',
  },
  thumb: {
    width: 100,
    height: 120,
    marginBottom: 5,
    backgroundColor: '#00282c',
  },
  title: {
    fontSize: 12,
    color: '#fff',
    fontWeight: '800',
  },
  subTitle: {
    fontSize: 13,
    color: '#3DDC84',
  },
});

export default MovieItem;
