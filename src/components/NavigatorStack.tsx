import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import {SCREEN_HOME , SCREEN_DETAIL}  from '../screens/routeConstant';

const NavigatorStack = () => {

  /** Create App StackNavigator **/
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={SCREEN_HOME} component={HomeScreen} />
      <Stack.Screen name={SCREEN_DETAIL} component={DetailScreen} />
    </Stack.Navigator>
  );
};

export default NavigatorStack;
