import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Provider as StateProvider} from 'react-redux';
import getStore from './src/redux/store';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import NavigatorStack from './src/components/NavigatorStack';

const App = () => {
  /** Hide Splash screen on app load **/
  React.useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <SafeAreaView style={styles.content}>
      <NavigationContainer>
        <StateProvider store={getStore()}>
          <NavigatorStack />
        </StateProvider>
      </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
});

export default App;
